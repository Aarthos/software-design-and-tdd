Feature: Convert F to C
  This feature takes a temperature value in Fahrenheit and displays temperature value in the appropriate Celsius unit.

Scenario: Page displays proper heading
  Given I visit the page for converting F to C
  When I view the content of the page
  Then I see Temperature Converter

Scenario: Temperature Input Field has 0 and no oC displayed
  Given I visit the page for converting F to C
  When I view the content of the page
  Then I see 0 for F in the input field and no oC

Scenario: Convert 50F to Celsius
  Given I visit the page for converting F to C
  When I enter 50 into the text box
  And click on the Convert Button
  Then I see 10oC

Scenario: Convert a few F to Celsius
  Given I visit the page for converting F to C
  When I enter <F> into the text box
  And click on the Convert Button
  Then I see <C>
  | F   | C    |
  | 32  | 0    |
  | 14  | -10  |
  | 77  | 25   |
  | 23  | -5   |
  | -13 | -25  |
  | -40 | -40  |  
