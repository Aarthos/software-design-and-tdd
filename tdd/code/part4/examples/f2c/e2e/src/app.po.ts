import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async getHeadingText(): Promise<string> {
    return element(by.css('h1')).getText();
  }

  async getInputFieldText(): Promise<string> {
    return element(by.css('input')).getAttribute('value');
  }
  
  async isTextPresent(text: string): Promise<boolean> {
    const exits = await element(by.css('span')).isPresent();
    
    if(exits) {
      const textContent = await element(by.css('span')).getText();
      return textContent.includes(text);
    } else {
      return false;
    }    
  }
  
  async enterTextIntoTextBox(text: string): Promise<unknown> {
    await element(by.css('input')).clear();
    return element(by.css('input')).sendKeys(text);
  }
  
  async clickOnConvert(): Promise<unknown> {
    return element(by.css('button')).click();
  }
  
  async getCelsius(): Promise<string> {
    return element(by.css('#celsius')).getText();
  }
}
