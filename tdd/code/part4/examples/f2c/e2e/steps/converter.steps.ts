import { Given, When, Then, TableDefinition } from 'cucumber';
import { AppPage } from '../src/app.po';
import { expect } from 'chai';

const page = new AppPage();

Given('I visit the page for converting F to C', async function () {
  await page.navigateTo();
});

When('I view the content of the page', async function () {
});

Then('I see Temperature Converter', async function () {
  expect(await page.getHeadingText()).to.equal('Temperature Converter');    
});

Then('I see 0 for F in the input field and no oC', async function () {
  expect(await page.getInputFieldText()).to.equal('0');    
  expect(await page.isTextPresent('C')).to.be.false;
});

When('I enter 50 into the text box', async function () {
  await page.enterTextIntoTextBox('50');
});

When('click on the Convert Button', async function () {
  await page.clickOnConvert();
});

Then('I see 10oC', async function () {
  expect(await page.getCelsius()).to.equal('10oC');
});

When('I enter <F> into the text box', async function () {
});

Then('I see <C>', async function (dataTable: TableDefinition) {
  for(const [f, c] of dataTable.rows()) {
    await page.enterTextIntoTextBox(f);
    await page.clickOnConvert();
    expect(await page.getCelsius()).to.equal(`${c}oC`);
  }
});
