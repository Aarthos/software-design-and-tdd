import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.css']
})
export class ConverterComponent implements OnInit {
  temperatureInCelsius = "";
  temperatureInFahrenheit = 0.0;
  
  constructor() { }

  ngOnInit(): void {
  }

  convert() {
    const inCelsius = (this.temperatureInFahrenheit - 32) * 5.0 / 9;
    
    this.temperatureInCelsius = `${inCelsius}`;
  }
}
