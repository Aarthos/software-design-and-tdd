package com.agiledeveloper;

public class TicTacToeLogic {
  public enum Peg { EMPTY, FIRST, SECOND };
  public enum GameStatus { PLAYING, WIN };

  private static final int SIZE = 3;

  private Peg nextPegToPlace = Peg.FIRST;
  private Peg[][] pegs = new Peg[SIZE][SIZE];

  public TicTacToeLogic() {
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        pegs[i][j] = Peg.EMPTY;
      }
    }
  }

  //  public void placePeg(int row, int column) {
//    //as written place peg violates the SRP and SLAP
//    pegs[row][column] = nextPegToPlace;
//
//    if(nextPegToPlace == Peg.FIRST) {
//      nextPegToPlace = Peg.SECOND;
//    } else {
//      nextPegToPlace = Peg.FIRST;
//    }
//  }

//  public void placePeg(int row, int column) {
//    pegs[row][column] = nextPegToPlace;
//
//    toggleNextPeg();
//  }
//
//  private void toggleNextPeg() {
//    nextPegToPlace = nextPegToPlace == Peg.FIRST ? Peg.SECOND : Peg.FIRST;
//  }

//or

  public void placePeg(int row, int column) {
    if(pegs[row][column] != Peg.EMPTY) {
      throw new RuntimeException("Can't place peg at an occupied position");
    }

    pegs[row][column] = nextPegToPlace;

    nextPegToPlace = toggleNextPeg();
  }

  private Peg toggleNextPeg() {
    return nextPegToPlace == Peg.FIRST ? Peg.SECOND : Peg.FIRST;
  }

  public Peg pegAtPosition(int row, int column) {
    return pegs[row][column];
  }

  public GameStatus getGameStatus() {
    GameStatus gameStatus = GameStatus.PLAYING;

    if(checkRowMatch()) {
      gameStatus = GameStatus.WIN;
    }

    return gameStatus;
  }

  private boolean checkRowMatch() {
    for (int i = 0; i < SIZE; i++) {
      if(pegs[i][0] != Peg.EMPTY && pegs[i][0] == pegs[i][1] && pegs[i][1] == pegs[i][2]) {
        return true;
      }
    }
    return false;
  }
}
