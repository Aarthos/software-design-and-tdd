package com.agiledeveloper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.agiledeveloper.TicTacToeLogic.Peg;
import com.agiledeveloper.TicTacToeLogic.GameStatus;

import static org.junit.jupiter.api.Assertions.*;

public class TicTacToeLogicTest {
  TicTacToeLogic ticTacToeLogic;

  @BeforeEach
  void init() {
    ticTacToeLogic = new TicTacToeLogic(); //give isolation between the tests by creating a fresh new object for each test FAIR
  }

  @Test
  void canary(){
    assertTrue(true);
  }

  @Test
  void firstPlayerPlacesAPeg(){
    //TicTacToeLogic ticTacToeLogic = new TicTacToeLogic(); //We will keep it DRY

    //ticTacToeLogic.placePeg(new Peg(new Player(1), "X"), new Cell(1, 2));
    //would that work?
    //do we know the size of the grid? Let us assume 3x3

    //to do the above, we have to write 3 classes, a bunch constructors, and a method, and
    //may be more. A small test, a boat load of code. Does not feel right.

    //That is awefully a lot of work. Tests should walk us incrementally step by step.
    //No giant little test and a lot of code practice.

    //Let's rethink...

    //The Player is not really necessary, at least in the logic. It may be needed in the GUI but
    //that is a separate concern elsewhere. YAGNI

    //ticTacToeLogic.placePeg(new Peg("X"), new Cell(1, 2));

    //that would again require Peg and Cell to be written now. How about?

    //ticTacToeLogic.placePeg(1, 2, "X");
    //Good new, we don't need a Cell or a Peg class at this time.
    //Bad news, we are using a String to represent a Peg and that is not clear from the modeling point of view.
    //We want to be domain specific and types are useful for that. Tension here between less vs. clarity

    //But wait, what is "X"? It is a representation in the GUI. Potentially we may create a game with
    //Goats and Chicken instead of X and O.

    //We really care that we placed a first peg, its GUI or visual representation can be moved to the
    //GUI instead of being here in the logic.

    ticTacToeLogic.placePeg(1, 2);

    assertEquals(Peg.FIRST, ticTacToeLogic.pegAtPosition(1, 2));

    //What did we do here?
    //We were writing some tests?
    //We were designing the skin of a function. We are not merely writing tests, we are in the act
    //of tactical design.

    //It is not write test and write code
    //It is write test, listen to it, think through the design, make changes to the test, until
    //we feel we have a good design in place to implement.

    //Kent Beck: Unit testing is more of an act of design than an act of verification.
  }

  @Test
  void secondPlayerPlacesAPeg(){
    //TicTacToeLogic ticTacToeLogic = new TicTacToeLogic();

    ticTacToeLogic.placePeg(1, 2);
    ticTacToeLogic.placePeg(2, 2);

    assertEquals(Peg.SECOND, ticTacToeLogic.pegAtPosition(2, 2));
  }

  @Test
  void firstPlayerPlacesASecondPeg(){
    ticTacToeLogic.placePeg(1, 2);
    ticTacToeLogic.placePeg(2, 1);
    ticTacToeLogic.placePeg(2, 2);

    assertEquals(Peg.FIRST, ticTacToeLogic.pegAtPosition(1, 2));
    assertEquals(Peg.SECOND, ticTacToeLogic.pegAtPosition(2, 1));
    assertEquals(Peg.FIRST, ticTacToeLogic.pegAtPosition(2, 2));
  }

  @Test
  void placePegAtAnOccupiedPosition(){
    ticTacToeLogic.placePeg(2, 1);

    RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> ticTacToeLogic.placePeg(2, 1));
    assertEquals("Can't place peg at an occupied position", runtimeException.getMessage());
  }

  @Test
  void placePegOutOfRange(){
//    assertThrows(IndexOutOfBoundsException.class, () -> ticTacToeLogic.placePeg(-2, 1));
//    assertThrows(IndexOutOfBoundsException.class, () -> ticTacToeLogic.placePeg(21, 1));
//    assertThrows(IndexOutOfBoundsException.class, () -> ticTacToeLogic.placePeg(1, -1));
//    assertThrows(IndexOutOfBoundsException.class, () -> ticTacToeLogic.placePeg(1, 11));

    //In the above, if the first assert fails, the other asserts will not run. That is a poorly written test.
    //Do not write independent asserts in one test.
    //If two asserts are related to one action, then that is OK
    //But, if two asserts are related to separate actions, then don't put them in one test in JUnit 4.

    //But, in JUnit 5, don't over use the following:
    //We can use assertAll to put independent asserts in one test. But, keep this minimum

    assertAll(
      () -> assertThrows(IndexOutOfBoundsException.class, () -> ticTacToeLogic.placePeg(-2, 1)),
      () -> assertThrows(IndexOutOfBoundsException.class, () -> ticTacToeLogic.placePeg(21, 1)),
      ()  -> assertThrows(IndexOutOfBoundsException.class, () -> ticTacToeLogic.placePeg(1, -1)),
      () -> assertThrows(IndexOutOfBoundsException.class, () -> ticTacToeLogic.placePeg(1, 11)));

    //In the above, if one test fails, the other tests in the assertAll group will still run.
    //Keep in mind, however, that the ticTacToeLogic instance is the same across all the asserts in this group.
    //Thus we loose some isolation and need to keep an eye on it.
  }

  @Test
  void checkRowMatch(){
    ticTacToeLogic.placePeg(1, 2);
    ticTacToeLogic.placePeg(2, 2);
    ticTacToeLogic.placePeg(1, 0);
    ticTacToeLogic.placePeg(2, 1);
    ticTacToeLogic.placePeg(1, 1);

    //we can run the functions like above, or if the sequence is long, we can directly set the
    //state in the object from the test (reaching into package friendly variables or methods)
    //and then perform the action of interest.

    assertEquals(GameStatus.WIN, ticTacToeLogic.getGameStatus());
  }

  @Test
  void gameNotOverAtStart(){
    assertEquals(GameStatus.PLAYING, ticTacToeLogic.getGameStatus());
  }
}
