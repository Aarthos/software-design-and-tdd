package com.agiledeveloper;

public class Calculator {
  public int add(int op1, int op2) {
    return op1 + op2;
  }

  public int divide(int numerator, int denominator) {
  //Joe changes the above to the below
  //public double divide(double numerator, double denominator) {
    return numerator / denominator;
  }

  //Will the compiler create any error when the ints were changed to doubles?
  //No error means.... ship it

  //But, a few days go by, we call Shashank on the phone and say "Hey, your code does not work."
  //Shashank says, "But I am on vacation, I did not touch the code in days."
  //We start debugging and then eventually find the problem.
  //
  //Floating point division does not throw exception when division by zero happens.
  //
  //But, if Joe had written the tests, then... the test would catch the issue well before
  //someone else finds the problem.

  //Unit tests are angels on my shoulder... they whisper that "my code sucks" well before
  //someone else look at it.

  //When we change a piece of code, unit tests can verify if the code still conforms to the
  //expected behavior.

  //Stuart Halloway (he said this years go, has not come true yet, but...): "In 10 years we will view unit tests as a form of compilation."
  //Just like we rely on the feedback from the compilers, we will rely on feedback from tests.
  //Compiler is the weakest form of unit testing.
}
