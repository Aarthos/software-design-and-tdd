package com.agiledeveloper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {
  @Test
  void addTwoNumbers(){
    Calculator calculator = new Calculator();

    assertEquals(6, calculator.add(2, 4));
  }

  @Test
  void divideANumber(){
    Calculator calculator = new Calculator();

    assertEquals(4, calculator.divide(8, 2));
  }

  //Suppose we do not write tests or enough tests. Then we are caught off guard when a change goes in a way other than planned

  //Suppose the following test does not exist
  @Test
  void divisionByZero(){
    Calculator calculator = new Calculator();

    Exception ex = assertThrows(ArithmeticException.class, () -> calculator.divide(6, 0));
    assertEquals("/ by zero", ex.getMessage());
  }
  //again, suppose the above test does not exist. Joe has written code without any tests.
  //A few weeks go by. Joe decides to make a change to the code.
}
