package game;

public class GameOfLife {
  public enum CellState { DEAD, ALIVE }
  private static final int SIZE = 10;

  public static CellState nextState(CellState currentState, int numberOfLiveNeighbors) {
    return numberOfLiveNeighbors == 3 || currentState == CellState.ALIVE && numberOfLiveNeighbors == 2 ?
      CellState.ALIVE : CellState.DEAD;
  }

  public static int numberOfLiveNeighborsOf(CellState[][] cells, int row, int column) {
    int count = 0;

    for(int i = row - 1; i <= row + 1; i++) {
      for(int j = column - 1; j <= column + 1; j++) {
        if(isAliveAt(cells, i, j)) {
          count++;
        }
      }
    }

    return isAliveAt(cells, row, column) ? count - 1 : count;
  }

  private static boolean isAliveAt(CellState[][] cells, int row, int column) {
    return row >= 0 && row < SIZE && column >= 0 && column < SIZE && cells[row][column] == CellState.ALIVE;
  }

  public static CellState[][] nextGeneration(CellState[][] universe) {
    CellState[][] nextGenerationUniverse = new CellState[SIZE][SIZE];

    for(int i = 0; i < SIZE; i++) {
      for(int j = 0; j < SIZE; j++) {
        nextGenerationUniverse[i][j] = nextState(universe[i][j], numberOfLiveNeighborsOf(universe, i, j));
      }
    }

    return nextGenerationUniverse;
  }
}
