package game;

import org.junit.jupiter.api.Test;

import java.util.List;

import static game.GameOfLife.CellState.DEAD;
import static game.GameOfLife.CellState.ALIVE;
import static game.GameOfLife.*;
import static org.junit.jupiter.api.Assertions.*;

public class GameOfLifeTest {
  @Test
  void canary(){
    assertTrue(true);
  }

//  @Test
//  void aDeadCellWithZeroLiveNeighborsStaysDead(){
//    //assertFalse(gameOfLife.isCellAlive(false, 0));
//    //WDYT?
//    //+Nice that we are not dealing with state right now
//    //-what does true/false mean?
//
//    //Remember Kent Beck's 4 Rules for simple design
//    //Code should reveal intention. true or false does not.
//
//    //Write code that is minimum but expressive
//
//    assertEquals(DEAD, nextState(DEAD, 0));
//  }

  @Test
  void deadCellBehavior(){
    assertAll(
      () -> assertEquals(DEAD, nextState(DEAD, 0)),
      () -> assertEquals(DEAD, nextState(DEAD, 1)),
      () -> assertEquals(DEAD, nextState(DEAD, 2)),
      () -> assertEquals(DEAD, nextState(DEAD, 5)),
      () -> assertEquals(DEAD, nextState(DEAD, 8)),
      () -> assertEquals(ALIVE, nextState(DEAD, 3))
    );
  }

  @Test
  void liveCellBehavior(){
    assertAll(
      () -> assertEquals(DEAD, nextState(ALIVE, 0)),
      () -> assertEquals(DEAD, nextState(ALIVE, 1)),
      () -> assertEquals(DEAD, nextState(ALIVE, 5)),
      () -> assertEquals(DEAD, nextState(ALIVE, 8)),
      () -> assertEquals(ALIVE, nextState(ALIVE, 2)),
      () -> assertEquals(ALIVE, nextState(ALIVE, 3))
    );
  }

  //AAA
  //Arrange
  //Act
  //Assert
  //give one blank line between each of these parts

  private CellState[][] createUniverse(List<Integer>... livePositions) {
    CellState[][] cells = new CellState[10][10];

    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 10; j++) {
        cells[i][j] = DEAD;
      }
    }

    for(List<Integer> livePosition: livePositions) {
      int row = livePosition.get(0);
      int column = livePosition.get(1);

      cells[row][column] = ALIVE;
    }

    return cells;
  }

  @Test
  void numberOfLiveNeighborsIs0(){
    CellState[][] cells = createUniverse();

    assertEquals(0, numberOfLiveNeighborsOf(cells, 5, 5));
  }
  //The first few tests help us to design the skin (the interface). More tests that follow
  //help us to design the guts (the implementation).

  @Test
  void numberOfLiveNeighborsIs1(){
    CellState[][] cells = createUniverse(List.of(5, 6));

    assertEquals(1, numberOfLiveNeighborsOf(cells, 5, 5));
  }

  @Test
  void numberOfLiveNeighborsIs1ForALiveCell(){
    CellState[][] cells = createUniverse(List.of(5, 6), List.of(5, 5));

    assertEquals(1, numberOfLiveNeighborsOf(cells, 5, 5));
  }

  @Test
  void numberOfLiveNeighborsIs2(){
    CellState[][] cells = createUniverse(List.of(5, 6), List.of(4, 5));

    assertEquals(2, numberOfLiveNeighborsOf(cells, 5, 5));
  }

  @Test
  void numberOfLiveNeighborsIs3(){
    CellState[][] cells = createUniverse(List.of(5, 6), List.of(4, 5), List.of(6, 4));

    assertEquals(3, numberOfLiveNeighborsOf(cells, 5, 5));
  }

  @Test
  void numberOfLiveNeighborsIs2WithAFarAwayNeighbor(){
    CellState[][] cells = createUniverse(List.of(5, 6), List.of(4, 5), List.of(1, 4));

    assertEquals(2, numberOfLiveNeighborsOf(cells, 5, 5));
  }

  @Test
  void numberOfLiveNeibhborsForTopLeftCell(){
    CellState[][] cells = createUniverse(List.of(0, 0), List.of(0, 1), List.of(1, 4));

    assertEquals(1, numberOfLiveNeighborsOf(cells, 0, 0));
  }

  @Test
  void numberOfLiveNeibhborsForTopRightCell(){
    CellState[][] cells = createUniverse(List.of(0, 9), List.of(0, 8), List.of(1, 8));

    assertEquals(2, numberOfLiveNeighborsOf(cells, 0, 9));
  }

  @Test
  void numberOfLiveNeibhborsForBottomLeftCell(){
    CellState[][] cells = createUniverse(List.of(9, 1), List.of(0, 8), List.of(1, 8));

    assertEquals(1, numberOfLiveNeighborsOf(cells, 9, 0));
  }

  @Test
  void numberOfLiveNeibhborsForBottomRightCell(){
    CellState[][] cells = createUniverse(List.of(9, 9), List.of(9, 8), List.of(1, 8));

    assertEquals(1, numberOfLiveNeighborsOf(cells, 9, 9));
  }

  @Test
  void aDeadUniverseStaysDead(){
    CellState[][] universe = createUniverse();

    assertArrayEquals(universe, nextGeneration(universe));
  }

  @Test
  void aUniverseWithALoneLiveCellBecomesDead(){
    CellState[][] universe = createUniverse(List.of(1, 1));

    assertArrayEquals(createUniverse(), nextGeneration(universe));
  }

  @Test
  void aUniverseWithTwoLiveCellsBecomesDead(){
    CellState[][] universe = createUniverse(List.of(1, 1), List.of(1, 2));

    assertArrayEquals(createUniverse(), nextGeneration(universe));
  }

  @Test
  void transformUniverseWithThreeLiveNeighbors(){
    CellState[][] universe = createUniverse(List.of(1, 1), List.of(1, 2), List.of(2, 2));

    assertArrayEquals(createUniverse(List.of(1, 1), List.of(1, 2), List.of(2, 2), List.of(2, 1)), nextGeneration(universe));
  }

  @Test
  void aBlockRemainsABlock(){
    CellState[][] universe = createUniverse(List.of(1, 1), List.of(1, 2), List.of(2, 2), List.of(2, 1));

    assertArrayEquals(createUniverse(List.of(1, 1), List.of(1, 2), List.of(2, 2), List.of(2, 1)), nextGeneration(universe));
  }

  @Test
  void aHorizontalBlinkerBecomesAVerticalBlinker(){
    CellState[][] universe = createUniverse(List.of(3, 2), List.of(3, 3), List.of(3, 4));

    assertArrayEquals(createUniverse(List.of(2, 3), List.of(3, 3), List.of(4, 3)), nextGeneration(universe));
  }

  @Test
  void aVerticalBlinkerBecomesAHorizontalBlinker(){
    CellState[][] universe = createUniverse(List.of(2, 3), List.of(3, 3), List.of(4, 3));

    assertArrayEquals(createUniverse(List.of(3, 2), List.of(3, 3), List.of(3, 4)), nextGeneration(universe));
  }
}
