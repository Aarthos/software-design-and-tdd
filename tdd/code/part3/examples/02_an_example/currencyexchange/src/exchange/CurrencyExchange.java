package exchange;

import java.util.Currency;

public interface CurrencyExchange {
  double getExchangeRate(Currency destinationCurrency);
}
