package exchange;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Optional;

public class CurrencyConverter {
  private List<CurrencyExchange> currencyExchanges = new ArrayList<>();

  public void addCurrencyExchange(CurrencyExchange currencyExchange) {
    currencyExchanges.add(currencyExchange);
  }

  public int computeAmountAfterProfit(int amountInCents, double commissionPercent) {
    return (int)(amountInCents - amountInCents * commissionPercent / 100);
  }

//  public int exchangeCurrency(int amountInCentsUSD, Currency destinationCurrency) {
//    final double COMMISSION_PERCENT = 2.0;
//
//    double conversionRate = 0;
//    boolean gotAConvertion = false;
//
//    for(CurrencyExchange currencyExchange: currencyExchanges) {
//      try {
//        conversionRate = Math.max(conversionRate, currencyExchange.getExchangeRate(destinationCurrency));
//        gotAConvertion = true;
//      } catch (Exception ex) {
//        //ignore with intention
//      }
//    }
//
//    if(!gotAConvertion) {
//      throw new RuntimeException("No service available");
//    }
//
//    int convertedAmount = (int) (conversionRate * amountInCentsUSD);
//
//    return computeAmountAfterProfit(convertedAmount, COMMISSION_PERCENT);
//  }

  //Refactor to follow SLAP, SRP,...

  private Optional<Double> computeMaxConversionRate(Currency destinationCurrency) {
    double conversionRate = 0;

    for(CurrencyExchange currencyExchange: currencyExchanges) {
      try {
        conversionRate = Math.max(conversionRate, currencyExchange.getExchangeRate(destinationCurrency));
      } catch (Exception ex) {
        //ignore with intention
      }
    }

    return conversionRate == 0 ? Optional.empty() : Optional.of(conversionRate);
  }

  public int exchangeCurrency(int amountInCentsUSD, Currency destinationCurrency) {
    final double COMMISSION_PERCENT = 2.0;

    Optional<Double> conversionRate = computeMaxConversionRate(destinationCurrency);

    if(conversionRate.isPresent()) {
      int convertedAmount = (int)(conversionRate.orElse(0.0) * amountInCentsUSD);
      return computeAmountAfterProfit(convertedAmount, COMMISSION_PERCENT);
    }

    throw new RuntimeException("No service available");
  }
}

