package exchange;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.*;

public class CurrencyConverterTest {
  private CurrencyConverter currencyConverter;
  private final Currency INR = Currency.getInstance("INR");

  @BeforeEach
  void init() {
    currencyConverter = new CurrencyConverter();
  }

  @Test
  void canary() {
    assertTrue(true);
  }

  @Test
  void convertAmountAfterProfitTaken(){
    double commissionPercent = 2.0;
    int amountInCents = 10000;

    assertEquals(9800, currencyConverter.computeAmountAfterProfit(amountInCents, commissionPercent));
  }

  @Test
  void convertAmountAfterProfitTakenForAnotherAmount(){
    double commissionPercent = 2.4;
    int amountInCents = 20000;

    assertEquals(19520, currencyConverter.computeAmountAfterProfit(amountInCents, commissionPercent));
  }

  //test for rounding issues goes here.

  @Test
  void getExchangeWhenThereAreNoServices(){
    int amountInCentsUSD = 10000;

    Exception ex = assertThrows(RuntimeException.class, () -> currencyConverter.exchangeCurrency(amountInCentsUSD, INR));

    assertEquals("No service available", ex.getMessage());
  }

//  class CurrencyExchangeStub implements CurrencyExchange {
//    @Override
//    public double getExchangeRate(Currency destinationCurrency) {
//      return 75.05;
//    }
//  }
//
//  @Test
//  void getExchangeWhenThereIsOneService(){
//    int amountInCentsUSD = 10000;
//
//    CurrencyExchangeStub currencyExchangeStub = new CurrencyExchangeStub();
//    currencyConverter.addCurrencyExchange(currencyExchangeStub);
//
//    assertEquals(735490, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
//  }
//
//  @Test
//  void getExchangeForAnotherAmountWhenThereIsOneService(){
//    int amountInCentsUSD = 20000;
//
//    CurrencyExchangeStub currencyExchangeStub = new CurrencyExchangeStub();
//    currencyConverter.addCurrencyExchange(currencyExchangeStub);
//
//    assertEquals(1470980, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
//  }
//
//  @Test
//  void getExchangeForAnotherRateWhenThereIsOneService(){
//    int amountInCentsUSD = 10000;
//
//    CurrencyExchangeStub currencyExchangeStub = new CurrencyExchangeStub();
//    currencyConverter.addCurrencyExchange(currencyExchangeStub);
//
//    assertEquals(490000, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
//  }
  //what should we do to make the above test pass?
  //We may pass an exchange rate to the constructor of the CurrencyExchangeStub but that
  //will make the stub more complex. Thus, that is a bad idea. So what to do?

//  @Test
//  void getExchangeWhenThereIsOneService(){
//    int amountInCentsUSD = 10000;
//
//    CurrencyExchange currencyExchangeStub = new CurrencyExchange() {
//      @Override
//      public double getExchangeRate(Currency destinationCurrency) {
//        return 75.05;
//      }
//    };
//
//    currencyConverter.addCurrencyExchange(currencyExchangeStub);
//
//    assertEquals(735490, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
//  }
//
//  @Test
//  void getExchangeForAnotherAmountWhenThereIsOneService(){
//    int amountInCentsUSD = 20000;
//
//    CurrencyExchange currencyExchangeStub = new CurrencyExchange() {
//      @Override
//      public double getExchangeRate(Currency destinationCurrency) {
//        return 75.05;
//      }
//    };
//
//    currencyConverter.addCurrencyExchange(currencyExchangeStub);
//
//    assertEquals(1470980, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
//  }
//
//  @Test
//  void getExchangeForAnotherRateWhenThereIsOneService(){
//    int amountInCentsUSD = 10000;
//
//    CurrencyExchange currencyExchangeStub = new CurrencyExchange() {
//      @Override
//      public double getExchangeRate(Currency destinationCurrency) {
//        return 50.00;
//      }
//    };
//
//    currencyConverter.addCurrencyExchange(currencyExchangeStub);
//
//    assertEquals(490000, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
//  }
  //The moral here is that we should not use one stub across all the tests. Instead we
  //should create one stub for each test. In other words, the stubs should be tailored for
  //meeting the needs of the particular test

  //But if we do that, we end up with noisy tests. This is where we can use a library, like
  //Mockito.

  @Test
  void getExchangeWhenThereIsOneService(){
    int amountInCentsUSD = 10000;

    CurrencyExchange currencyExchangeStub = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub);

    when(currencyExchangeStub.getExchangeRate(INR)).thenReturn(75.05);

    assertEquals(735490, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
  }

  @Test
  void getExchangeForAnotherAmountWhenThereIsOneService(){
    int amountInCentsUSD = 20000;

    CurrencyExchange currencyExchangeStub = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub);

    when(currencyExchangeStub.getExchangeRate(INR)).thenReturn(75.05);

    assertEquals(1470980, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
  }

  @Test
  void getExchangeForAnotherRateWhenThereIsOneService(){
    int amountInCentsUSD = 10000;

    CurrencyExchange currencyExchangeStub = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub);

    when(currencyExchangeStub.getExchangeRate(INR)).thenReturn(50.00);

    assertEquals(490000, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
  }

  @Test
  void getExchangeWhenThereAreTwoServicesWithFirstGivingHigherRate(){
    int amountInCentsUSD = 10000;

    CurrencyExchange currencyExchangeStub1 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub1);

    CurrencyExchange currencyExchangeStub2 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub2);

    when(currencyExchangeStub1.getExchangeRate(INR)).thenReturn(75.05);
    when(currencyExchangeStub2.getExchangeRate(INR)).thenReturn(50.00);

    assertEquals(735490, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
  }

  @Test
  void getExchangeWhenThereAreTwoServicesWithSecondGivingHigherRate(){
    int amountInCentsUSD = 10000;

    CurrencyExchange currencyExchangeStub1 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub1);

    CurrencyExchange currencyExchangeStub2 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub2);

    when(currencyExchangeStub1.getExchangeRate(INR)).thenReturn(50.00);
    when(currencyExchangeStub2.getExchangeRate(INR)).thenReturn(75.05);

    assertEquals(735490, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
  }

  @Test
  void getExchangeWhenThereAreTwoServicesWithBothHaveTheSameRate(){
    //consult with the domain experts as to how to handle it. They may have a preference...
    //Suppose they told us pick the last one with hither rate.

    int amountInCentsUSD = 10000;

    CurrencyExchange currencyExchangeStub1 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub1);

    CurrencyExchange currencyExchangeStub2 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub2);

    when(currencyExchangeStub1.getExchangeRate(INR)).thenReturn(50.00);
    when(currencyExchangeStub2.getExchangeRate(INR)).thenReturn(50.00);

    assertEquals(490000, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
  }

  @Test
  void getExchangeWhenThereAreTwoServicesWithTheFirstOneFailing(){
    int amountInCentsUSD = 10000;

    CurrencyExchange currencyExchangeStub1 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub1);

    CurrencyExchange currencyExchangeStub2 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub2);

    when(currencyExchangeStub1.getExchangeRate(INR)).thenThrow(new RuntimeException("Network failure"));
    when(currencyExchangeStub2.getExchangeRate(INR)).thenReturn(50.00);

    assertEquals(490000, currencyConverter.exchangeCurrency(amountInCentsUSD, INR));
  }

  //we can continue to write other tests for the different scenarios we identified in the tests.txt file.

  @Test
  void getExchangeWhenThereAreThreeServicesButAllFailing(){
    int amountInCentsUSD = 10000;

    CurrencyExchange currencyExchangeStub1 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub1);

    CurrencyExchange currencyExchangeStub2 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub2);

    CurrencyExchange currencyExchangeStub3 = mock(CurrencyExchange.class);
    currencyConverter.addCurrencyExchange(currencyExchangeStub3);

    when(currencyExchangeStub1.getExchangeRate(INR)).thenThrow(new RuntimeException("Network failure"));
    when(currencyExchangeStub2.getExchangeRate(INR)).thenThrow(new RuntimeException("Network failure"));
    when(currencyExchangeStub3.getExchangeRate(INR)).thenThrow(new RuntimeException("Network failure"));

    Exception ex = assertThrows(RuntimeException.class, () -> currencyConverter.exchangeCurrency(amountInCentsUSD, INR));

    assertEquals("No service available", ex.getMessage());
  }
}
