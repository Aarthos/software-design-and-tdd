package com.agiledeveloper.airport;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class FAAAirportInfoServiceTest {
  private FAAAirportInfoService fAAAirportInfoService;
  private FAAAirportInfoService faaAirportInfoServiceMock;

  String validJson = "{\"Name\":\"George Bush Intercontinental/houston\",\"City\":\"Houston\",\"State\":\"TX\",\"ICAO\":\"KIAH\",\"IATA\":\"IAH\",\"SupportedAirport\":false,\"Delay\":false,\"DelayCount\":0,\"Status\":[{\"Reason\":\"No known delays for this airport\"}],\"Weather\":{\"Weather\":[{\"Temp\":[\"Partly Cloudy\"]}],\"Visibility\":[10.00],\"Meta\":[{\"Credit\":\"NOAA's National Weather Service\",\"Url\":\"http://weather.gov/\",\"Updated\":\"Last Updated on Feb 24 2019, 6:53 am CST\"}],\"Temp\":[\"49.0 F (9.4 C)\"],\"Wind\":[\"North at 11.5\"]}}";

  String invalidJson = "{\"SupportedAirport\":false,\"Delay\":false,\"DelayCount\":0,\"Status\":[{\"Type\":\"\",\"AvgDelay\":\"\",\"ClosureEnd\":\"\",\"ClosureBegin\":\"\",\"MinDelay\":\"\",\"Trend\":\"\",\"MaxDelay\":\"\",\"EndTime\":\"\"}]}";

  @BeforeEach
  public void init() {
    fAAAirportInfoService = new FAAAirportInfoService();
    faaAirportInfoServiceMock = mock(FAAAirportInfoService.class);
  }

  //This test alone is an integration test. We may want to place this test in a separate test
  //suite, in a different directory and run it only as part of integration test run.
  @Test
  public void getJsonReturnsValidJsonFromTheServiceGivenAirportCode() throws IOException {
    String jsonResponse = fAAAirportInfoService.getJSON("IAH");

    assertTrue(jsonResponse.contains(
      "\"Name\":\"George Bush Intcntl/houston\""));

    assertTrue(jsonResponse.contains("\"City\":\"Houston\""));

    assertTrue(jsonResponse.contains("\"State\":\"TX\""));

    assertTrue(jsonResponse.contains("\"Weather\":{\"Weather\":[{\"Temp\":"));

    assertTrue(jsonResponse.contains("\"Delay\":"));
  }

  @Test
  public void createAirportGivenValidJSON() {
    Airport airport = fAAAirportInfoService.createAirport(validJson);

    assertEquals("IAH", airport.getCode());
    assertEquals("George Bush Intercontinental/houston", airport.getName());
    assertEquals("Houston", airport.getCity());
    assertEquals("TX", airport.getState());
    assertEquals("49.0 F (9.4 C)", airport.getTemperature());
    assertEquals("false", airport.getDelay());
  }

  @Test
  public void getExceptionWithNoValidNameInJson() {
    assertThrows(RuntimeException.class,
      () -> fAAAirportInfoService.createAirport(invalidJson));
  }

  //If we write a test on fetchData which calls getJSON, that test will become an integration test
  //To make this a unit test we stubbed service's getJSON function and then call the real fetchData.
  //Thus this test became a unit test instead of being an integration test.
  //This stub behaves more like a spy as far as the fetchData but behaves as a stub for the getJSON
  @Test
  public void fetchDataCallsGetJSON() throws IOException {
    when(faaAirportInfoServiceMock.getJSON("IAH")).thenReturn(validJson);
    when(faaAirportInfoServiceMock.fetchData("IAH")).thenCallRealMethod();

    faaAirportInfoServiceMock.fetchData("IAH");

    verify(faaAirportInfoServiceMock, times(1))
      .getJSON("IAH");
  }

  @Test
  public void fetchDataCallsCreateAirportWithResponseFromGetJSON() throws IOException {
    when(faaAirportInfoServiceMock.getJSON("IAH")).thenReturn(validJson);
    when(faaAirportInfoServiceMock.createAirport(validJson)).thenReturn(new Airport("George Bush"));
    //whereas we need a stub for getJSON (not idempotent) we could have used the real createAirport function since that is idempotent
    //if the computation for createAirport, parsing, were to take more time or resource, that may be a good reason to stub as we did here.

    when(faaAirportInfoServiceMock.fetchData("IAH")).thenCallRealMethod();

    faaAirportInfoServiceMock.fetchData("IAH");

    verify(faaAirportInfoServiceMock, times(1))
      .createAirport(validJson);
  }

  @Test
  public void fetchDataReturnsAirportReturnedByCreateAirport() throws IOException {
    Airport sampleAirport = new Airport("");
    when(faaAirportInfoServiceMock.getJSON("IAH")).thenReturn(validJson);
    when(faaAirportInfoServiceMock.createAirport(validJson)).thenReturn(sampleAirport);
    when(faaAirportInfoServiceMock.fetchData("IAH")).thenCallRealMethod();

    assertEquals(sampleAirport, faaAirportInfoServiceMock.fetchData("IAH"));
  }

  @Test
  public void fetchDataPropagatesExceptionFromCreateAirport() throws IOException {
    when(faaAirportInfoServiceMock.getJSON("IAH")).thenReturn(invalidJson);
    doThrow(RuntimeException.class).when(faaAirportInfoServiceMock).createAirport(invalidJson);
    when(faaAirportInfoServiceMock.fetchData("IAH")).thenCallRealMethod();

    assertThrows(RuntimeException.class,
      () -> faaAirportInfoServiceMock.fetchData("IAH"));

    verify(faaAirportInfoServiceMock, times(1)).createAirport(invalidJson);
  }

  @Test
  public void fetchDataPropagatesExceptionFromGetJSON() throws IOException {
    when(faaAirportInfoServiceMock.getJSON("IAH")).thenThrow(IOException.class);
    when(faaAirportInfoServiceMock.fetchData("IAH")).thenCallRealMethod();

    assertThrows(RuntimeException.class,
      () -> faaAirportInfoServiceMock.fetchData("IAH"));

    verify(faaAirportInfoServiceMock, times(1)).getJSON("IAH");
  }
}
