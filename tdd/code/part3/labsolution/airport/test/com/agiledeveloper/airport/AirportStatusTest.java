package com.agiledeveloper.airport;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AirportStatusTest {
  private AirportStatus airportStatus;

  private AirportStatusService statusService;

  private Airport iah = new Airport("George Bush Intercontinental/houston");
  private Airport jfk = new Airport("John F. Kennedy Intl");
  private Airport bom = new Airport("Chtrapati Shivaji");

  @Test
  public void canary() {
    assertTrue(true);
  }

  @BeforeEach
  public void init() {
    airportStatus = new AirportStatus();
    statusService = mock(AirportStatusService.class);
    airportStatus.setStatusService(statusService);
  }

  @Test
  public void sortingListOfAirportsReturnsSortedList() {
    assertAll(
      () -> assertEquals(List.of(), airportStatus.sortAirports(List.of())),
      () -> assertEquals(List.of(iah),
        airportStatus.sortAirports(List.of(iah))),
      () -> assertEquals(List.of(iah, jfk),
        airportStatus.sortAirports(List.of(iah, jfk))),
      () -> assertEquals(List.of(iah, jfk),
        airportStatus.sortAirports(List.of(jfk, iah))),
      () -> assertEquals(List.of(bom, iah, jfk),
        airportStatus.sortAirports(List.of(bom, iah, jfk))),
      () -> assertEquals(List.of(bom, iah, jfk),
        airportStatus.sortAirports(List.of(iah, bom, jfk))));
  }

  @Test
  public void passingEmptyListOfCodesReturnsEmptyListOfAirports() {
    assertEquals(List.of(), airportStatus.getAirportStatus(List.of())
      .airports);
  }

  @Test
  public void passingListWithOneCodeReturnsListWithOneName() {
    when(statusService.fetchData("IAH")).thenReturn(iah);

    assertEquals(List.of(iah),
      airportStatus.getAirportStatus(List.of("IAH"))
        .airports);

    //if we want to verify the interaction, then we can do the following:
    //verify(statusService, times(1)).fetchData("IAH");
    //But, even without this, as long as the code is calling the service,
    //the stub will return the canned response only if the argument passed
    //is as stated in the when.

    //Then when should we use a mock instead of a stub.
    //1. Where sufficient, use stub
    //2. If you want to verify the number of calls (more than 1), then use mock/verify
    //3. If you are worried a call is not even made to the dependency, then use mock/verify
  }

  @Test
  public void passingListWithTwoCodesReturnsListWithTwoNames() {
    when(statusService.fetchData("IAH")).thenReturn(iah);
    when(statusService.fetchData("JFK")).thenReturn(jfk);

    assertEquals(List.of(iah, jfk),
      airportStatus.getAirportStatus(List.of("IAH", "JFK"))
        .airports);
  }

  @Test
  public void passingListWithTwoCodesReturnsSortedListWithTwoNames() {
    when(statusService.fetchData("IAH")).thenReturn(iah);
    when(statusService.fetchData("JFK")).thenReturn(jfk);

    assertEquals(List.of(iah, jfk),
      airportStatus.getAirportStatus(List.of("JFK", "IAH"))
        .airports);
  }

  @Test
  public void passingListWithThreeCodesReturnsSortedListWithThreeNames() {
    when(statusService.fetchData("IAH")).thenReturn(iah);
    when(statusService.fetchData("JFK")).thenReturn(jfk);
    when(statusService.fetchData("BOM")).thenReturn(bom);

    assertEquals(List.of(bom, iah, jfk),
      airportStatus.getAirportStatus(List.of("JFK", "BOM", "IAH"))
        .airports);
  }

  @Test
  public void oneAirportCodeIsInvalid() {
    when(statusService.fetchData("IAH")).thenThrow(RuntimeException.class);

    assertEquals(List.of("IAH"),
      airportStatus.getAirportStatus(List.of("IAH")).airportCodesWithError);
  }

  @Test
  public void twoAirportCodesWhereSecondIsInvalid() {
    when(statusService.fetchData("IAH")).thenReturn(iah);
    when(statusService.fetchData("JFK")).thenThrow(RuntimeException.class);

    var result = airportStatus.getAirportStatus(List.of("IAH", "JFK"));

    assertEquals(List.of(iah), result.airports);

    assertEquals(List.of("JFK"), result.airportCodesWithError);
  }

  @Test
  public void threeAirportCodesWhereFirstIsInvalidAndThirdGivesNetworkError() {
    when(statusService.fetchData("IAH")).thenThrow(RuntimeException.class);
    when(statusService.fetchData("BOM")).thenThrow(RuntimeException.class);
    when(statusService.fetchData("JFK")).thenReturn(jfk);

    var result = airportStatus.getAirportStatus(List.of("JFK", "IAH", "BOM"));

    assertEquals(List.of(jfk), result.airports);

    assertEquals(List.of("IAH", "BOM"), result.airportCodesWithError);
  }
}
