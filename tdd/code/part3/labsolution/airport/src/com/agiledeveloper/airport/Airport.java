package com.agiledeveloper.airport;

public class Airport {
  private final String name;
  private final String code;
  private final String city;
  private final String state;
  private final String temperature;
  private final String delay;

  Airport(String airportName) {
    this(airportName, "", "", "", "", "");
  }

  Airport(String airportName, String airportCode, String airportCity,
          String airportState, String airportTemperature, String airportDelay) {
    name = airportName;
    code = airportCode;
    city = airportCity;
    state = airportState;
    temperature = airportTemperature;
    delay = airportDelay;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public String getTemperature() {
    return temperature;
  }

  public String getDelay() {
    return delay;
  }
}
