package com.agiledeveloper.airport;

import java.util.List;

public class AirportStatusResult {
  public final List<Airport> airports;
  public final List<String> airportCodesWithError;

  AirportStatusResult(
    List<Airport> validAirports, List<String> invalidAirportsCodes){
    airports = validAirports;
    airportCodesWithError = invalidAirportsCodes;
  }
}
