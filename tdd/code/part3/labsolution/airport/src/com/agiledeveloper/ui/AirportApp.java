package com.agiledeveloper.ui;

import com.agiledeveloper.airport.Airport;
import com.agiledeveloper.airport.AirportStatus;
import com.agiledeveloper.airport.AirportStatusResult;
import com.agiledeveloper.airport.FAAAirportInfoService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class AirportApp {
  static void printValidAirport(Airport airport) {
    System.out.println(String.format(
      "%-40s %-20s %-10s %-20s %-10s",
      airport.getName(), airport.getCity(), airport.getState(),
      airport.getTemperature(), getDelaySymbol(airport)
    ));
  }

  public static String getDelaySymbol(Airport airport){
    String delay = airport.getDelay();
    if(delay.equals("false"))
      return "";
    return "º";
  }

  static List<String> getAirportCodes(String fileName) throws IOException {
    return Files.lines(Paths.get(fileName)).collect(toList());
  }

  static AirportStatus setFAAService() {
    var faaService = new FAAAirportInfoService();
    var statusService = new AirportStatus();
    statusService.setStatusService(faaService);

    return statusService;
  }

  static AirportStatusResult getResults(List<String> airportCodes) {
    return setFAAService().getAirportStatus(airportCodes);
  }

  static void printResults(AirportStatusResult result) {
    System.out.printf("%-40s %-20s %-10s %-20s %-10s \n", "Name", "City", "State", "Temperature", "Delay");
    System.out.println("-".repeat(100));


    result.airports
      .forEach(AirportApp::printValidAirport);

    if (!result.airportCodesWithError.isEmpty()) {
      System.out.println("\n" + "-".repeat(100));

      System.out.println("Error getting details for:");
      System.out.println("-".repeat(100));

      result.airportCodesWithError
        .forEach(System.out::println);
    }
  }

  public static void main(String[] args) {
    try {
      var airportCodes = getAirportCodes(args[0]);

      printResults(getResults(airportCodes));
    } catch (IOException ex) {
      System.out.println("Input file not found. Sorry!");
    }
  }
}
