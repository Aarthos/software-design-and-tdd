package com.agiledeveloper.airport;

import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;

public class AirportStatus {
  private AirportStatusService statusService;

  public void setStatusService(AirportStatusService theStatusService) {
    statusService = theStatusService;
  }

  List<Airport> sortAirports(List<Airport> airports) {
    return airports.stream()
      .sorted(comparing(Airport::getName))
      .collect(toList());
  }

  public AirportStatusResult getAirportStatus(List<String> airportCodes) {
    List<Airport> airports = new ArrayList<>();
    List<String> airportCodesWithError = new ArrayList<>();

    for(var code: airportCodes) {
      try {
        airports.add(statusService.fetchData(code));
      }catch(Exception ex) {
        airportCodesWithError.add(code);
      }
    }

    return new AirportStatusResult(sortAirports(airports), airportCodesWithError);
  }
}
