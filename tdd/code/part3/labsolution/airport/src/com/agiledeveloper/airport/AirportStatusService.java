package com.agiledeveloper.airport;

public interface AirportStatusService {
  Airport fetchData(String airportCode);
}
