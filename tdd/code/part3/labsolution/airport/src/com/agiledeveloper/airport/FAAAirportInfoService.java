package com.agiledeveloper.airport;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class FAAAirportInfoService implements AirportStatusService {
  String getJSON(String airportCode) throws IOException {
    var faaURL = "https://soa.smext.faa.gov/asws/api/airport/status/" + airportCode;

    try (Scanner scanner = new Scanner(new URL(faaURL).openStream())) {
      return scanner.nextLine();
    }
  }

  Airport createAirport(String jsonResponse) {
    JsonObject airportObj = new Gson().fromJson(jsonResponse, JsonObject.class);

    String airportName = airportObj.get("Name").getAsString();
    String airportCode = airportObj.get("IATA").getAsString();
    String airportCity = airportObj.get("City").getAsString();
    String airportState = airportObj.get("State").getAsString();
    String airportTemperature = airportObj.get("Weather").getAsJsonObject().get("Temp").getAsString();
    String airportDelay = airportObj.get("Delay").getAsString();

    return new Airport(airportName, airportCode, airportCity, airportState,
      airportTemperature, airportDelay);
  }

  public Airport fetchData(String airportCode) {
    try {
      return createAirport(getJSON(airportCode));
    }catch(IOException ex) {
      throw new RuntimeException(ex);
    }
  }
}
