Agile Software Design: Principles, Practices, Patterns by Robert Martin

Building Evolutionary Architectures: Support Constant Change by Neal Ford, Rebecca Parsons, Patrick Kua

Test Driven Development: By Example by Kent Beck

Growing Object-Oriented Software, Guided by Tests by Steve Freeman, Nat Pryce

The Checklist Manifesto: How to Get Things Right by Atul Gawande

