In this exercise we will create a strategic design for a program. 
Please show the design using the UML notation.

We will write a zero player program to implement the Game of Life:
https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life

The program will start with an input of a list of live cells. It will, at one second interval, display the cells that are live and dead. Each second, it will display the next generation of cells.

For the purpose of this lab we can assume the universe if finite in size where the size is specified when the program starts.

We will not write any code in this lab, that will come later. For this lab, please create only a strategic design, identify the parts of the program we will create to implement this game.

            *
Game  <>-----> Cell
Cell
State

              *
Universe <>-----> Cell

Game <>----> Orchestrator (does the generation - the timer)

What about the GUI

But, what is Game?

