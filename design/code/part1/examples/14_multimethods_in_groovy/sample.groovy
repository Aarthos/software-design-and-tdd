class Base {
  public void foo(double a) {
    System.out.println("Base.foo(double) called");
  }
}

class Derived extends Base {
  public void foo(double a) {
    System.out.println("Derived.foo(double) called");
  }
  public void foo(int a) {
    System.out.println("Derived.foo(int) called");
  }
}

void use(Base base) {
    base.foo(4); 
}

use(new Base());  
use(new Derived());  

/*
We pretty much copied and pasted the code from Sample.java to
sample.groovy. Remove just a few things. Ran the code, but
the output is different.

whereas Java gave us

Base.foo(double)
Derived.foo(double)

Groovy says
Base.foo(double)
Derived.foo(int)

That is multimethod, polymorphism on steroids.

The method that is called is based both on the runtime type of
the target and the runtime type of the parameter.
*/
