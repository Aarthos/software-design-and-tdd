class Base {
  public void foo(double a) {
    System.out.println("Base.foo(double) called");
  }
}

class Derived extends Base {
  public void foo(double a) {
    System.out.println("Derived.foo(double) called");
  }
  public void foo(int a) {
    System.out.println("Derived.foo(int) called");
  }
}

public class Sample {
  public static void use(Base base) {
    base.foo(4); 
    //what should I call. 
    //Is there a foo in Base?
    //Yes, it is virtual (not final) so do not bind to it. You may
    //have to call one in the derived.
    //But, what does the foo take?
    //Oh, a double. OK, then, convert 4 to double at compile time.
    //At runtime, call a foo, on the runtime type of base, by passing
    //a double 4.0.
  }

  public static void main(String[] args) {
    use(new Base());  //Base.foo(double)
    use(new Derived()); //Derived.foo(double) even though there is a Derived.foo(int). That is polymorphism.
  }
}
