class Man {
  help() {
    console.log(`Man is helping`);
  }
}

class Woman {
  help() {
    console.log(`Woman is helping`);
  }
}

class Elephant {
  help() {
    console.log(`Elephant is helping`);
  }
}

class Cat {
}

function seekHelp(helper) {
  try {
    helper.help()
  } catch(ex) {
    console.log(ex.message);
  }
}

seekHelp(new Man())
seekHelp(new Woman())
seekHelp(new Elephant())
seekHelp(new Cat())

/*
  Design by capability instead of design by contract

  If you have the ability to fullful an expectation you can use that
  instance. What if it does not? Results in runtime failures that
  you may have to handle gracefully

  Design by contact is really good in parts of the system where
  we need strong verfication and conformance.

  Design by capability is really good in parts of the system where
  we want flexiblity and dynamic behavior without the efforts to
  extend an hierarchy.

  We can reuse code by using inheritance or delegation, but that is
  a choice here and we are not forced to use it in design by
  capability unlike in design by contract.

  How can we do design by capability in Java?
  1. You can't
  2. You can achieve this using reflection, but not that desirable
  3. Use Groovy if we twist the question as on the JVM instead of Java
*/

