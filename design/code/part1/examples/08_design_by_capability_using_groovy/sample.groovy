class Man {
  def help() { println 'Man is helping' }
}

class Woman {
  def help() { println 'Woman is helping' }
}

class Elephant {
  def help() { println 'Elephant is helping' }
}

class Cat {
}


def seekHelp(helper) {
  try {
   helper.help()
  } catch(Exception ex) {
    println ex.message
  }
}

seekHelp(new Man())
seekHelp(new Woman())
seekHelp(new Elephant())
seekHelp(new Cat())

