import java.util.*;

//Suppose we have a class Man
class Man {
  public void help() { System.out.println("Man is helping"); }
}

public class Sample {
  //Suppose we want to seek help

  public static void seekHelp(Man helper) {
    helper.help();
  }

  public static void main(String[] args) {
    Man man = new Man();

    seekHelp(man);
  }
}

