import java.util.*;

interface Helper {
  default void help() {
    System.out.println(getClass().getSimpleName() + " is helping");
  }
}

//Suppose we have a class Man
class Man implements Helper {
}

class Woman implements Helper {
}

public class Sample {
  //Suppose we want to seek help

  public static void seekHelp(Helper helper) {
    helper.help();
  }

  public static void main(String[] args) {
    Man man = new Man();
    seekHelp(man);

    Woman woman = new Woman();
    seekHelp(woman);
  }
}

