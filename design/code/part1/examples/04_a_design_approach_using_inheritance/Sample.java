import java.util.*;

interface Helper {
  void help();
}

//Suppose we have a class Man
class Man implements Helper {
  public void help() { System.out.println("Man is helping"); }
}

class Woman implements Helper {
  public void help() { System.out.println("Woman is helping"); }
}

public class Sample {
  //Suppose we want to seek help

  public static void seekHelp(Helper helper) {
    helper.help();
  }

  public static void main(String[] args) {
    Man man = new Man();
    seekHelp(man);

    Woman woman = new Woman();
    seekHelp(woman);
  }
}

//That works, but may lead to duplication of code
