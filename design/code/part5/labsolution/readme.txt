A developer has decided to create the following design.

class Rectangle {
  private double length;
  private double width;
  
  public double getLength() { return length; }
  public void setLength(double value) { length = value; }
  
  public double getWidth() { return width; }
  public void setWidth(double value) { width = value; }  
}

class Square extends Rectangle {
  public void setLength(double value) {
    setWidth(value);
  }
  
  public void setWidth(double value) {
    super.setLength(value);
    super.setWidth(value);
  }  
}

Is this a good design or are there issues with it?

The only good part of this design is that it is trying to keep it DRY.

However, it violates the LSP, limits the functionality of the Rectangle in the implementation of Square.

Issues:
-models Square is a Rectangle which means we can pass a Square anywhere a Rectangle is used.
  The users of a Rectangle are assuming that a Square behaves and follows the same rules of a Rectangle.
  Thus, they may query for the length, modify the width, and assume that the area or the perimeter
  is what they know it to be. But once we change the length or the width, unlike a Rectangle, a Square
  change both the dimensions. LSP says the derived class methods should require no more and promise no less
  than the corresponding methods of the base class. The derived methods do not conform to the expectations
  of the base methods.

  A user of a Rectangle is going to be confused why when they change length it affected the width when a 
  Square is passed in.

The users of a rectangle in this case may report a bug because behavior is different from what they expect.
Or, they have to check for the instanceof - violating OCP.

Square violates the expectations or the contract of the base properties which says they are independent of each other.

In this case, delegation may be better than inheritance if reuse is essential.

Design is full of tradeoffs. We have to evaluate and compromise.

Expectations matter.

