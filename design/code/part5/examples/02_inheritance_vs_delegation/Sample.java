import java.util.*;

public class Sample {
  public static void main(String[] args) {
  }
}

/*
Inheritance is where one class extends from another class, carrying
over the interface (what) and the implementation (how) from one
to the other.

The subclass instances have all the properties or fields of the base
class in its memory.

class Animal {
  private String name;
}

class Dog extends Animal {
  private int age;
}

The memory for a Dog instance contains both name and age. Internally 
it contains the Animal part and the Dog part.

Delegation is where one class has a reference to an instance of another
class. The instance of the first class makes use of the instance of
the second class, to implement part of its own behavior.

Inheritance is rather static in most languages. Once we inherit from
a class, we are stuck to it. We can't change the inheritance dynamically
at runtime in most languages.

Delegation is dynamic in most languages. We can change the instance
and the type of the instance at runtime.

class Vehicle {}
class Car extends Vehicle {}

Car is bound to Vehicle. - static

class Person {
  Animal pet;
}

The pet may be a Dog, Cat, etc. - dynamic

Delegation is more flexible than inheritance.

In fact, if we look at most of the design patterns in the GOF book,
most of the patterns use delegation as primary solution more than
using inheritance.

Let us pick a few patterns to discuss:
Abstract Factory, Factory Method, Decorator, Adaptor, Composite, 
Strategy, Iterator, ...


Inheritance is the main approach
Factory Method
Adapter (class based)

Delegation is the main approach
Abstract Factory
Decorator
Composite
Strategy
Iterator
Adapter (object based)

*/

