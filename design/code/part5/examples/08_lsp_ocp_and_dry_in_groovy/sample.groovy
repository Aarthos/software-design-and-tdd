class Programmer {
  def work() { println 'coding...' }
}

class Manager {
  @Delegate Programmer programmer = new Programmer()
}

def bob = new Manager()
bob.work() //coding...

//The Groovy compiler, upon seeing @Delegate write the work method in the
//Manager class, at the bytecode level, and routes the call to the programmer
//instance. In other word, the compiler does what we manually did in Java.

//DRY - we do not have to duplicate the effort the compiler does the duplication 
//instead

//OCP - if we modify the Programmer, the compiler will rewrite the Manager's
//methods accordinly and we don't have to. So, the Manager code is closed from 
//change to the Programmer.

//We honor LSP, OCP, DRY and our users are not affected either.

