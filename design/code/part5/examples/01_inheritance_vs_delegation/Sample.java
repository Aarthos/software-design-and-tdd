import java.util.*;

public class Sample {
  public static void main(String[] args) {
  }
}

/*
Inheritance:

superclass and subclass
base classes and derived classes

Inheritance models an is-a or kind-of relationship

We want to model one abstraction as a specialization of another abstraction

The difference between the base/super and the derived/subclass is called
a discriminator

class Animal {}
class Dog extends Animal {}
class Cat extends Animal {}

The discriminator here is the characteristics of a Dog that is different
from the characteristics of a Cat, even though they both are kind-of animals.

Delegation:

We have a few different kinds of relationships:

Association: --------->
Aggregation: <>------->
Composition: <x>------>

If we were to use OO terms, we can say that composition is more of
a specialized kind-of aggregation and aggregation is a more of a
specialized kind-of association.

Delegation is more of a general term that ways an object is related to
another object (via association, aggregation, or composition) and is
interested in delegating or handing over part of the responsibility
to that related object.
*/
