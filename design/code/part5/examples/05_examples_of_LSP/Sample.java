import java.util.*;

public class Sample {
  public static void main(String[] args) {
  }
}

/*
Places where LSP is used nicely:

1. When overriding a method in Java, the derived class can't have
a stricter access control for the overriding method than the
control in the base class.

base public method ---  derived can only have public for that method
base protected method - derived can have public or protected for that method, but no private.

In C#, they require the access control to be the same.

Why?

Because if we don't users of a base will have trouble when an instance
of derived is passed.

2. When overriding a method, the derived class method is not allowed
to throw any new checked exceptions that is not thrown by the
base method (with a small exception).


2. When overriding a method, the derived class method is not allowed
to throw any new checked exceptions that is not thrown by the
base method (with a small exception).

this is enforced by the Java compiler, but we should go fruther than
that.

If a derived throws arbitrary exceptions, then the user who relies
on the contract of the base will be surprised at the behavior of the
code when a derived instance is passed in.

A place where LSP is violated:

A well defined class should preserve invariants

But, in the JDK,

class Stack extends Vector - oops

This makes it possible to pass a Stack where a Vector is expected.
That can violate the invariant of a Stack.
A stack honor a LIFO, you add to the top and remove from the top.
But, a vector allows insertions and deletions anywhere. We don't want
that for a Stack, but using stack as a vector makes that possible.

What they should have done is

class Stack {
  private Vector vector...
}

instead of

class Stack extends Vector {...

*/
