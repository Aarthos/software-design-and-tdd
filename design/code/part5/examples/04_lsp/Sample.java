import java.util.*;

public class Sample {
  public static void main(String[] args) {
  }
}

/*
Using inheritance so we can write less code is trouble

The correctness of the modeling is important than the number of lines

If your goal is to reuse, use delegation. If you goal is to substitute
then use inheritance.

Why

Barbara Liskov's Substitution Principle

The methods of a derived class should require no more and promise
no less than the corresponding methods of the base class. In other words,
the users of a base class should not be able to tell the difference
between when they user a base instance or a derived instance.

If we pass an instance of a derived class to a function that expects
an instance of the base class, then the function should be able to work
with the instance passed without ever wondering what the type the
instance is.

Why? Why does LSP really insist on that? What if we don't honor it?

If we do not honor that, that is we write the derived class in any
way we like without regard to how the base works, then the users
of the base class will have to check if they got a base or one
of the derived types and change how they interact.

If we violate LSP then the users of our code will violate the OCP

LSP really helps us design in a way that we do not cause issues
to the users of our hierarchy.

Implementing inheritance is not easy. We have to make sure that every 
single method of the derived class conforms tot he expected behavior
(documented, intended, assumed, perceived...) of the corresponding methods of the base class.

Creating delegation is far easier than creating inheritance.

Delegation is more flexible and less risky than inheritance.

This is not to say we should not use inheritance. It is to emphasize
that we should evaluate thoroughly every single time and decide which
is a better option.

Use inheritance only when we want substitutability an make sure to conform
to the expectations of the base class.

*/
