import java.util.*;

class Programmer {
  public void work() {
    System.out.println("coding...");
  }
}

class Manager extends Programmer {
}

//We want to be able to reuse the programmer's work in the manager

//One easy solution that comes to mind is inheritance.
//But, is this a good choice?

//We want  a Manager to use a programmer but do we also want a manager to be used
//as a programmer.

public class Sample {
  public static void doWork(Programmer programmer) {
   programmer.work();
  }

  public static void main(String[] args) {
    Manager bob = new Manager();
    doWork(bob);
  }
}

//This is unlikely the design we really wanted.

//Benefit: we did not have to write the work method in the Manager. We are keeping
//it DRY.

//Drawback: The Manager can be used as a Programmer. Now, the Manager has to conform
//to the expectations and the contracts of a Programmer class.
//If we don't then we will end up violating LSP and the users of our class will
//have to check if the instance given is a programmer or a Manager and take
//action accordingly and not rely on polymorphism. Thus the user will be violating the
//OCP.

//When using inheritance, if we violate LSP users of our classes will en up violating
//OCP.

//What gives?

