import java.util.*;

public class Sample {
  public static void main(String[] args) {
  }
}

/*
When should we use inheritance and when to use delegation?

If an object of a class B wants to use an object of class A,
then use delegation.

If we want to use an object of B where an object of A is used,
then use inheritance.

To use (delegation) vs. to be used as or in place of (inheritance)

If our goal is to reuse, use delegation.
If our goal is substitutability, then use inheritance.


class Erase {} //helps to erase something and has an erase method

We want to model an EfficientEraser which will have an erase method
but erase more efficiently.

Question: Should we use inheritance or delegation?

Do we want to use an EfficientEraser where an Eraser is expected.
If yes, then inheritance.

We want to model an Instructor who will have an erase method to
erase at anytime the board is cluttered.

Question: Should we use inheritance or delegation?

Do we want to use an Instructor where an Eraser is expected.
I hope not.

class Instructor extends Eraser {} //bad idea
since a student can pick up this poor instructor and erase the
board with.

We want delegation here. The Instructor uses the Eraser

class Instructor {
  Erase eraser;

  public Instructor(Eraser eraser) {}
    this.eraser = erase;
  }

  public void erase() {
    pick up the eraser
    erase.erase();
    drop the eraser
  }
}

*/
