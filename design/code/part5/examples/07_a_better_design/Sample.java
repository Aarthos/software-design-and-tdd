import java.util.*;

class Programmer {
  public void work() {
    System.out.println("coding...");
  }
}

class Manager {
  private Programmer programmer = new Programmer(); //tight coupling

  public void work() {
    programmer.work();
  }
}

//This is using delegation instead of inheritance

public class Sample {
  public static void doWork(Programmer programmer) {
   programmer.work();
  }

  public static void main(String[] args) {
    Manager bob = new Manager();
    //doWork(bob); //ERROR

    bob.work(); //we can reuse the Programmer's work on the Manager object.
  }
}

//Benefits: We can't treat a Manager as a Programmer, but we can reuse the Programmer's
//work in the implementation of the Manager. We used delegation and thus we have
//a better time modeling the Manager. The Manager does not have to conform to the
//behavior, expectations, or the contracts of the programmer, and we can more freely
//implement the Manager.

//We have no risk of the users of our classes violating OCP because of our design smells

//The dependency of Manager on the Programmer is better in this design than in
//the case of inheritance. In the case of inheritance, the Manager depends tightly on
//Programmer, whereas here, we can pass the Programmer as a constructor argument
//or via a setter and thus the Manager may delegate to a Programmer or a derived
//class of a Programmer.

//Drawbacks:  We are violating two principles in our own code here:
//DRY: because we are writing the work method only to merely route the call to the Programmer
//OCP: If we change the Programmer's work method, to take a different argument or change
//its signature or its return type, then we will also have to change the Manager. Thus
//the Manager is never closed to changes of the Programmer.

//Is this a better design?

//So, should we violate LSP or should we violate OCP and DRY instead?

//Choices that life sometimes gives us:

//ideally, none. :)

//but if we have no other options but to choose, which should we choose?

//violate LSP or violate both OCP and DRY?

//It is better to violate OCP and DRY because ?

//If we violate DRY and OCP, the users of our code are not affected.
//If we violate LSP, then the users of our code will violate OCP.

//Sometimes we have to take a hard path to make the life of others better.

//Of course, if there is a way not to violate any of these principles that will be
//great.

//Sadly, Java has a syntax for inheritance (in fact two: implements and extends).
//There is no keyword or special syntax for delegation.

//Groovy, for example, handles this using an annotation and pre-compilation process
//What about Java?

//Project Lombok was influenced by Groovy and brings those ideas to Java, if you 
//plan to use that library.

