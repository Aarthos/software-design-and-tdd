import java.util.*;

public class Sample {
  public static void main(String[] args) {
  }
}

/*
What is extensibility?

We like for a piece of code to accommodate a reasonable change in requirement
without much of code change.

What causes a piece of code to change more frequently?
Vioaltes SRP
Low cohesion
High or tight coupling

If an order processor depends on a VisaPaymentMethod, then we will have to change
the code for the order processor if we decide to accept a different form of payment.
Tight coupling leading to poor extensible.

OCP: A module should be open for extension but closed for modification

A piece of code can't be arbitrarily extensible. We have to plan for what to
make the code extensible for.

Abstraction and polymorphism are the key.

Suppose we have a Car class. If we are expected to have different types of Engines
we can then make the code extensible by not tightly coupling to a particular 
type of Engine. Instead, we can use an interface or an abstract base class of Engine
and receive different types of implementations for the Engine interface or 
abstract base class.

But, if we suddenly say that we want a back up engine for a car, then the
code is not extensible for that. Why not? We simply did not anticipate and thus
design for it.

Design can't be arbitrarily extensible.

Extensibility requires deliberate planning, effort, and evaluation.
Also, there is a cost to accommodate extensibility. It can make the design more
complex. Ask if that is really needed.
*/
