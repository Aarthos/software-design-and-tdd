/*
In the lab, solution 1 was nice and simple, textbook example of DIP

Solution 2, on the other hand, looked weird and different. 

When we try something different, naturally, we look for conformance, we look for
case studies, we want to know others have done it and are still living to tell the story.

Surprisingly the solution 2 is sooooooo common we don't even realize we have
use it sooo extensively.

As it turns out, that is what is used both in Java and C# for iterators.
*/

import java.util.*;

public class Sample {
  public static void process(Iterator<Integer> iterator) {
    while(iterator.hasNext()) {
      System.out.println(iterator.next());
    }

    System.out.println("Curious:");
    System.out.println(iterator.getClass());
  }

  public static void main(String[] args) {
    List<Integer> numbers1 = Arrays.asList(1, 2, 3);
    Set<Integer> numbers2 = new HashSet<>(Arrays.asList(1, 2, 3));

    process(numbers1.iterator());
    System.out.println("-----");
    process(numbers2.iterator());

    System.out.println("==========");
    Iterator<Integer> iterator1 = numbers1.iterator();
    Iterator<Integer> iterator2 = numbers1.iterator();

    System.out.println(iterator1.next()); 
    System.out.println(iterator1.next());  // pointing to element 3 after the call
    System.out.println(iterator2.next());  // pointing to element 2 after the call
    System.out.println(iterator1.next()); 

    //iterator1 and iterator2 are two different views into the same list
  }
}

/*
With solution 2, we would have had List and Set implement a common interface for
iteration. But, here we ask for the iterator from List and Set.

the iterator() method on List and Set is like getRemote on TV and Fan and getARemote
on the Garage.

In Java, iterators are inner classes ($). In C#, they are nested classes (+)

Why did Java and C# take this route, rather than providing an interface for iteration
directly on the List or Set?

Efficiency and views.
By making this inner class, they can access the internals directly but at the
same time, we can have multiple concurrent iterations on the same list or set.

*/
