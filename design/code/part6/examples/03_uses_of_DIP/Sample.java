import java.util.*;

public class Sample {
  public static void main(String[] args) {
  }
}

/*
We have seen DIP is some places where it has been very helpful.

Database connectors
  We write code using JDBC library of interfaces but the implementations are provided
  by different vendors

Plugin architecture we use interface to interact with multiple plugin implementations

Use use this in Observer and Mediator patterns.

We let an algorithm decide part of its implementation delegating to an interface
that provides the variation - Strategy

When we use tools like Mockito, etc. we often use interfaces and provide stubs or
mocks instead of real objects.

Test ----> Code under Test  ------> Interface
                                        ^
					_
					|
					|
					|
                              --------------------------------
			      |                              |
                            Stub/Mock                    Real implementation

*/
