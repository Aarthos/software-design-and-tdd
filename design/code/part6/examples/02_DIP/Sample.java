import java.util.*;

public class Sample {
  public static void main(String[] args) {
  }
}

/*
Tight coupling makes testing and extensibility hard.

Let's say we have

Car ---> TurboEngine

We have a dependency of a Car on a concrete class TurboEngine.

If in the future the Car has to use a different Engine, it has to change. OCP violation.

First, what is the business requirement. If the requirement never wants us to use
any other type of Engine, then don't bother fixing a problem that does not exist.

But, if the business say, yes we have plans to use different types of Engines, then
plan for extensibility. 

Business requirements always should be kept in mind.

Car -----> Engine(interface) <|------ TurboEngine

Dependency Inversion Principle.
It states that a class should not depend on another class. Instead, they both
should depend on an interface.

Take that with a huge grain of salt. Don't use that blindly.

What does inversion mean here?

Instead of Car depending on TurboEngine, now both Car and TurboEngine depend on
Engine. The direction of dependency has been inverted, rather than dependency
going into TurboEngine, it is now going out.

We can use hand coded approach, Guice, Spring, etc. for dependency injection.

If we have used patterns like Abstract Factory, Decorator, Chain of Responsibility,
Strategy, Iterator, etc. we have used DIP.

We have used DIP when we used stubs and mocks to replace an interface.

Depending on an interface is loose coupling. Depending on a class is tight coupling.

As much as possible depend on interfaces rather than on class.

But, Ralph Johnson (coauthor of the Design Patterns book GOF) says
"we design hierarchy of classes and interface to make the design extensible, but
in doing so, we create code that is hard to extend."

While DIP is a great principle, keep in mind trade-offs, complexity, parsimony,
minimalistic design. Don't assume we have to use an interface everywhere.
*/

