package com.agiledeveloper;

public interface RemoteOperatable {
  public void up();
  public void down();
}