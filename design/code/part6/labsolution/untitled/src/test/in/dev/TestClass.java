package test.in.dev;

import main.GameOfLife;
import main.GameOfLife.State;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/*  Any live cell with fewer than two live neighbours dies, as if by underpopulation.
    Any live cell with two or three live neighbours lives on to the next generation.
    Any live cell with more than three live neighbours dies, as if by overpopulation.
    Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    These rules, which compare the behavior of the automaton to real life, can be condensed into the following:

    Any live cell with two or three live neighbours survives.
    Any dead cell with three live neighbours becomes a live cell.
    All other live cells die in the next generation. Similarly, all other dead cells stay dead.*/

/*
1. Cells are initialized properly.
2. We need to check whether each cell in the grid has a value, either alive or dead.
3. We need to define boundary.
4. Test to verify whether all fetched neighbours are valid neighbours along with boundary conditions
5.
*/
1 2 3
4 5 6
7 8 9


public class TestClass {

  GameOfLife gameOfLife;
  int[][] neighbours = new int[][]{{-1 -1},{-1, 0},{-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};

  @Before
  public void setup() {
    gameOfLife = new GameOfLife();
  }

  @Test
  public void testForInitialization() {
    Assert.assertNotEquals(State.EMPTY, gameOfLife.getPosition(1,1));
  }

  public void testForValidNeighbours() {
    int[] neighbours = gameOfLife.fetchNeighbours(1, 1, 1, neighbours);


  }



}
