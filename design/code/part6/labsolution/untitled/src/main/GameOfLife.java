package main;

public class GameOfLife {

  public enum State {
    ALIVE, DEAD, EMPTY
  }

  private final int SIZE = 3;
  private State[][] grid = new State[SIZE][SIZE];

  public GameOfLife() {
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        if (j%2 == 0) {
          grid[i][j] = State.DEAD;
        } else {
          grid[i][j] = State.ALIVE;
        }
      }
    }
  }

  public

  public State getPosition(int i, int j) {
    return grid[i][j];
  }

}
