package com.agiledeveloper;

public interface Appliance {
  void up();
  void down();
}
