import java.util.*;

public class Sample {
  public static void main(String[] args) {
  }
}

/*
Examples of OCP:

1. We should not make a field public within a class. Why?

If we decide to change the variable name or its implementation then
every single class in the application that uses it will have to change.

The users of the class are never closed from modification to this class.

This is why I refused to allow protected fields in a class. I insist on
writing private fields and protected accessors so the derived classes
are not violating OCP when they inherit from my class.

2. Do not use any global variables (in Java, via a static field). Why?

Just about any code in the application is no longer closed to the change
to that global variable.

3. Back in 1997, Stroustrup refused to add "dynamic casting" to C++
until languages like Java become popular. Eventually, dynamic casting,
which is called RTTI (Runtime Type Identification), was added to C++
very reluctantly but with a compiler flag to enable it, so as to
discourage programmers from making too much use of it.

The equivalent of that in Java is instanceof

Using instanceof may be a violation of OCP.

We saw this in the example of playWith earlier.

If a piece of code has to check the type of an instance at runtime
to be among one of many types, then when we extend the hierarchy
we will have to modify that code. Violates OCP.

But... read on... not all uses of instanceof are violation...
*/
