import java.util.*;

interface class Animal {
  void play();
}

class Dog implements Animal {
  public void play() {
    System.out.println("play with a frisbee");
  }
}

class Cat implements Animal {
  public void play() {
    System.out.println("play with laser pointers");
  }
}

class Person {
  public void playWith(Animal pet) {
    pet.play();
  }
}

interface PaymentMethod {
  PaymentStatus process(PaymentDatail detail);
}

class Order {
  public void processPayment(PaymentMethod paymentMethod, PaymentDatail detail) {
    //do some initial work here...

    paymentMethod.process(details);

    //do some addition work here...
  }
}

//Visa implements PaymentMethod and does things its way
//Mastercar implements PaymentMethod and does things its own way

public class Sample {
  public static void main(String[] args) {
  }
}

