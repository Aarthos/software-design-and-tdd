import java.util.*;

public class Sample {
  public static void main(String[] args) {
    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append("hello");

    System.out.println(stringBuilder.toString());
  }
}

/*
What is the worst keyword in Java from the point of view of extensibility?

 new > static > instanceof > if/else > switch > final > private 

 What does new mean? It means tight coupling

 The code new StringBuilder() depends directly on the class StringBuilder.

 An entire set of design patterns exist just to workaround that one issue of tight coupling that comes from the use of new.

 When we say new, we have to provide, in Java, C#, C++, the exact class that we want to instantiate. Tight coupling.

 In Abstarct Factory pattern, for example, we use polymorphism to call a factory function that in turn will call new to create the instance of a type. Level of indirection to solve the problem of tight coupling.o

 It is even worse.

 When that was not enough, we brought in an entire slue of framework
 to deal with new.

 Guice and Spring

 But, in languages like Ruby, we don't use patterns like Abstract Factor, etc. Because new is polymorphic in Ruby. In Python there is no "new" keyword. We treat the consturctor as a function and call it to create an object and thus we can be polymorphic quite easy.
*/

/*
 Here is a Ruby example:

class Book
end

class Song
end

def create(klass)
  klass.new #new is polymorphic
end

puts create(Book)
puts create(Song)

*/
