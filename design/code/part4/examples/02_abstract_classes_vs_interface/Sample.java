import java.util.*;

/*
abstract class Runner {
  private int distanceRan;

  public void run(int distance) {
    System.out.println("running...");
    distanceRan += distance;
  }

  public int getDistanceRan() { return distanceRan; }

}

class CasualRunner extends Runner {
}

class ProfessionalRunner extends Runner {
}
*/
//Good news the common code is the base class.
//If abstract methods are in the base, we will have to implement them.
//That gives us a chance to enhance the behavior if we like.
//But, if we have a different runner who needs an entire different
//implementation of how the distance is kept, or the format, etc.

//There is one more problem. What if a runner is not just a person.
//What if we have a robot runner. A Robot may extend from another class.
//Now, it can't extend from the Runner and that other class.
//ABC limit how we can extend, a class may have at most one base class
//but can have multiple interfaces that they implement.

//we wrap the state around methods that provide access to the state
//from the interface into the derived classes.

//interface  has methods ---> calls methods to access state
//classes implement interface and provide access methods for the state

//A class can now implement many interface and yet provide specialized
//implementation states as approriate.

interface Runner {
  default void run(int distance) {
    System.out.println("running...");
    updateDistance(distance);
  }

  int getDistanceRan();
  void updateDistance(int distance);
}

class CasualRunner implements Runner {
  private int distanceRan;

  public int getDistanceRan() { return distanceRan; }
  public void updateDistance(int distance) {
    distanceRan += distance;
  }
}

class ProfessionalRunner implements Runner {
  private int distance;

  public int getDistanceRan() { return distance; }
  public void updateDistance(int distance) {
    this.distance += distance;
  }
}

public class Sample {
  public static void use(Runner runner) {
    runner.run(10);

    System.out.println(runner.getDistanceRan());
  }

  public static void main(String[] args) {
    Runner aRunner1 = new CasualRunner();
    use(aRunner1);

    Runner aRunner2 = new ProfessionalRunner();
    use(aRunner2);
  }
}

/*
In Java we have interfaces, abstract base classes, also interfaces
with default methods.

As much as possible, it is better to use interfacees than abstract classes.

Interfaces can have different implementations without conforming to any restructions on implementations.

One reason, in the past, we liked abstract base classes is that they can have common function implementations. But, today, we have static and default methods in interfaces.

The key difference between abstract base classes and interfaces today is that abstract base classes can have non-final fields, but interfaces can't.

Is this a limitation? Not really. We can model around taht so that the
implementations can carry the details while the interface can still carry
the common code.
*/
