import java.util.*;

class Animal {}
class Dog extends Animal {}
class Cat extends Animal {}

class Person {
  public void playWith(Animal pet) {
    if(pet instanceof Dog) {
      System.out.println("play with a frisbee");
    }

    if(pet instanceof Cat) {
      System.out.println("play with laser pointers");
    }
  }
}

class PaymentMethod {}

class Order {
  public void processPayment(String paymentMethod, PaymentDatail detail) {
    if(paymentMethod.equals("Visa")) {
      //...
    }

    if(paymentMethod.equals("Mastercard")) {
      //...
    }
  }
}

public class Sample {
  public static void main(String[] args) {
  }
}

/*
We may receive a different type of object than what we are handling.

We may perform different actions for each type of object.

The code above is not following SRP. It is not cohesive. It violates
TDA principle. It is procedural in nature.

This code is not extensible.

In order to accommodate a new type in the system we will have to change
the code that deals with that type outside.

Open-Closed Principle (OCP):

Bertrand Meyer: A software module (class, function, component) should be
open for extension but closed from modification.

We should be able to extend a piece of code by adding small new code
and not by changing much of existing code.

The playWith function violate OCP. We can't open it for extension to play with another type of animal without also changing it.

Likewise, the processPayment violates OCP.


*/
