package com.agiledeveloper;

//abstract public class Engine {
//  public Engine() {}
//  protected Engine(Engine other) {}
//
//  public abstract Engine clone();
//}

import java.lang.reflect.Constructor;

abstract public class Engine {
  public Engine() {}
  protected Engine(Engine other) {}

  public Engine clone() {
    try {
      Constructor constructor = getClass().getDeclaredConstructor(getClass());
      constructor.setAccessible(true);
      return (Engine) constructor.newInstance(this);
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }
}
