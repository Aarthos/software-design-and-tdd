package com.agiledeveloper;

/*
In the give code the copy constructor was violation OCP using new and instanceof
If we add a new type of Engine, that will have to change.

How to solve?
First thought: use clone

In the past I used to recommend that we clone. But, then I read Effective Java.
The book recommended not to use clone. Why?

If a class has final fields, then clone can't provide a new value for those fields
when a copy is made.

What does the book then recommend?

They ask us to use copy constructor.

eh?

But we know that copy constructor does not work as we saw here.

We are stuck?

It turns out the problem is not with clone, but with the way clone is done in Java.
This is a Java issue, not a clone issue.

Solution, is use your own clone and do a double dispatch to your own protected copy
constructor.

Let's try.

Good news, Car is now not violating OCP. If we add another type of Engine, we don't
have to change the Car as the clone will take care of bring in the correct instance.

We made the constructor protected because we don't want uses of our classes to directly
use the copy constructor, using new, but to use clone which is polymorphic. But, then
why did we write a copy constructor? That is because we really want clone, but clone
can't set final fields. So clone uses the copy constructor as an implementation workaround
to be able to set final field. So users of the hierarchy should go through clone and not the
copy constructor.

What do you think?

But.... while the code is honoring OCP, it is violating DRY.

Every single Engine implementation has to provide a clone.

How can we fix that?

Somehow we have to use a common implementation.

Use reflection.

We honor both OCP and DRY.
 */
public class Car {
  private final int year; //please keep this final
  private final Engine engine; //please keep this final

  public Car(int theYear, Engine theEngine) {
    year = theYear;
    engine = theEngine;
  }

  protected Car(Car other) {
    year = other.year;

//    if(other.engine instanceof TurboEngine) {
//      engine = new TurboEngine((TurboEngine)(other.getEngine()));
//    } else {
//      engine = new PistonEngine((PistonEngine) (other.getEngine()));
//    }

    engine = other.engine.clone();
  }

  public Car clone() {
    return new Car(this);
  }

  public static Car copyCar(Car car) {
    //return new Car(car);
    return car.clone();
  }

  public int getYear() { return year; }
  public Engine getEngine() { return engine; }
}