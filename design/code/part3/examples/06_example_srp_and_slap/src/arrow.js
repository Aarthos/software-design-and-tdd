const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const result = 
  numbers.filter(number => number % 2 === 0)
    .map(number => number * 2)
    .reduce((total, number) => total + number);
   
console.log(result);

//Carries over the benefits from the previous functional.js version
//It is less noisy and a little bit easier to read.

//Honors cohesion, each line focuses on one thing and easier to
//read and understand.

//But, the arrow functions, still take a little bit of parsing through
//our human eyes and understanding what they do by reading the code.
