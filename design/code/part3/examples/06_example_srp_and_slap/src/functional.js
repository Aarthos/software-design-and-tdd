const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const result = 
  numbers.filter(function(number) { return number % 2 === 0; })
    .map(function(number) { return number * 2; })
    .reduce(function(total, number) { return total + number; });
   
console.log(result);

//This is a notch better than the verboe imperative style
//The code flows like the problem statement and in a single pass we get it

//Each line focuses on one single responsibility

//We can add and remove lines more easily to add or remove behavior

//The functions are still quite verbose and require time and effort to
//read and understand.
