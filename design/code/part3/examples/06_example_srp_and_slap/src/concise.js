const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const isEven = number => number % 2 === 0;
const double = number => number * 2;
const sum = (total, number) => total + number;

const result = 
  numbers.filter(isEven)
    .map(double)
    .reduce(sum);
   
console.log(result);

//names represent abstractions and communicate quicky, when done well,
//the purpose of a function or a variable, etc.

//Each line is very easy to understand without having to parse 
//through the details. The names reveal the intention quickly.

//The functions focus on a single level of detail, this code honors
//SRP and SLAP the most.
