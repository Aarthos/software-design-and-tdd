Tell, don't ask principle

Delegate the responsibilities to an object rather than querying for
the properties of an object and making the decisions outside of that
object for that object.

Consider that we have a person.

We want to decide if a person should vote, and if so they should vote.

class Person {
  public int getAge() ...
  public void vote(Election election) ...
}

There is a class that decides if the person is eligible to vote.

askPersonToVote(Person person, Election election) {
  if(person.getAge() > 17)) {
    person.vote(election);
  }
}

This is a violation of the tell, don't ask principle.

It asks and then tells.

Why is this not a good idea? After all it seems to work...

What if we say a person with criminal records can't vote...

askPersonToVote(Person person, Election election) {
  if(person.getAge() > 17) && !person.hasCriminalRecord()) {
    person.vote(election);
  }
}

Wait a minute, but that is not the rule in all states?

askPersonToVote(Person person, Election election) {
  if(person.getAge() > 17) && (!person.hasCriminalRecord() &&
    person.livesInState...) {
    person.vote(election);
  }
}

It turns out people living in some parts of a country may vote in some
elections but not in other.

The details of if person can vote really can get complex depending
on the person, where they live, their citizen status, various other details...

askPersonToVote(Person person, Election election) {
  person.voteIfEligible(election);
}

What is it that voteIfEligible gives that person.getAge() check does not?

Polymorphism

A person or a variation of the person (depending on citizen, state, etc.)
can decide what to do rather than that logic being procedural in
the askPersonToVote.

As much as possible delegate the responsibilities to objects instead of
querying and acting on behalf of the objects.



