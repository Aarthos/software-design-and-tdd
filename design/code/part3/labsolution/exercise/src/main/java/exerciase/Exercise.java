package exercise;

import java.util.*;
import static java.util.stream.Collectors.*;

//The design smells in the given code before any changes?
//The code fails the SLAP principle - we have multiple levels
//of details or abstractions in the code, in the loops, if,...

//duplication of the get for the same index - DRY

//Poor name for functions

//imperative and the flow is not obvious - SRP, Cohesion, ...

//may be unnecessary code - YAGNI

//we can refactor for the better.

public final class Exercise {
  public static String commaSeparatedNamesOfLength(List<String> names, int length) {
    return names.stream()
      .filter(name -> name.length() == length)
      .map(String::toUpperCase)
      .collect(joining(", "));
  }

  //this function needs a better name
  public static double calculate(List<Integer> numbers) {
    return numbers.stream()
      //.filter(number -> isEven(number) && isGT3(number) && isSQRTLT3(number))
      // please avoid the above - the below honors SRP. Each line is cohesive. We 
      //can easily add and remove logic. Easy to change and evolve. Easier to
      //read as well
      .filter(Exercise::isEven)  
      .filter(Exercise::isGT3)
      .filter(Exercise::isSQRTLT3)
      .mapToDouble(Exercise::doubleValue)
      .sum();
  }
  
  private static boolean isEven(int number) { return number % 2 == 0; }
  private static boolean isGT3(int number) { return number > 3; }
  private static boolean isSQRTLT3(int number) { return Math.sqrt(number) < 3; }
  private static int doubleValue(int number) { return number * 2; }
}
